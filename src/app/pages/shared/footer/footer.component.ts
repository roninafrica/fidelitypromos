import { Component, OnInit } from '@angular/core';

@Component({
	selector: "app-footer",
	templateUrl: "./footer.component.html",
	styleUrls: ["./footer.component.css"],
})
export class FooterComponent implements OnInit {
	constructor() {}

	currentDate = Date.now();

	Social: any[] = [
		{
			link:
				"https://business.facebook.com/fidelitybankgh/?business_id=653971478048365",
			icon: "fa-facebook-square",
		},
		{
			link: "https://twitter.com/fidelitybankgh",
			icon: "fa-twitter-square",
		},
		{
			link: "https://www.instagram.com/fidelitybankgh/",
			icon: "fa-instagram-square",
		},
		{
			link: "https://www.youtube.com/channel/UCMqrD-Wa4iMHcatw_CBC_Yg",
			icon: "fa-youtube",
		},
	];

	ngOnInit() {}
}
