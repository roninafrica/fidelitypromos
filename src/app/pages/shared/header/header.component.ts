import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
	selector: "app-header",
	templateUrl: "./header.component.html",
	styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
	constructor() {}

	nav = {
		logo: "assets/img/logo.png",
	};

	navLinks: any[] = [
		// {
		// 	name: "Home",
		// 	link: "/home",
		// },
	];

	public isCollapsed = true;
	ngOnInit() {
		$(document).ready(function () {
			$(window).scroll(function () {
				var scroll = $(window).scrollTop();
				if (scroll > 0) {
					$(".navbar").addClass("navbar-scroll");
				} else {
					$(".navbar").removeClass("navbar-scroll");
				}
				if (scroll > 200) {
					$(".navbar").addClass("bg-main");
				} else {
					$(".navbar").removeClass("bg-main");
				}
			});
		});
	}
}
