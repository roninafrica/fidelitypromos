import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './pages/views/pagenotfound/pagenotfound.component';
import { HomepageComponent } from './pages/views/homepage/homepage.component';


const routes: Routes = [
	{
		path: "",
		component: HomepageComponent,
	},
	{
		path: "home",
		component: HomepageComponent,
	},
	{
		path: "**",
		component: PagenotfoundComponent,
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
